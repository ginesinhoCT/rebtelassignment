//
//  RBTCountryInfoTests.m
//  RebtelAssignment
//
//  Created by Gines Sanchez Merono on 17/1/17.
//  Copyright © 2017 Ginés Sanchez. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "RBTCountryInfo.h"
#import "RBTConstants.h"

@interface RBTCountryInfoTests : XCTestCase

@end

@implementation RBTCountryInfoTests

- (void)setUp {
    [super setUp];
    // Put setup code here. This method is called before the invocation of each test method in the class.
}

- (void)testDefaultInitizer {
    RBTCountryInfo* countryInfo = [RBTCountryInfo new];
    XCTAssertNotNil(countryInfo, @"Cannot find RBTCountryInfo instance");
    XCTAssertEqualObjects([countryInfo fullName], @"Colombia (Colombia)", @"fullName failed.");
    XCTAssertEqualObjects([countryInfo code], @"CO", @"code failed.");
    XCTAssertEqualObjects([countryInfo capital], @"Bogotá", @"capital failed.");
    XCTAssertEqualObjects([countryInfo formattedPopulation], @"48,266,600", @"population failed.");
    XCTAssertEqualObjects([countryInfo fullRegion], @"Americas (South America)", @"fullRegion failed.");
}

- (void)testInitizerWithDictionary {
    NSDictionary * countryInfoDictionary = @{ kwCountryName : @"Spain",
                                              kwCountryNativeName : @"España",
                                              kwCountryCode : @"ES",
                                              kwCountryCapital : @"Madrid",
                                              kwCountryPopulation : @46439864,
                                              kwCountryRegion : @"Europe",
                                              kwCountrySubRegion : @"Southern Europe"
                                              };
    
    RBTCountryInfo* countryInfo = [[RBTCountryInfo alloc] initWithCountryInfo:countryInfoDictionary];
    XCTAssertNotNil(countryInfo, @"Cannot find RBTCountryInfo instance");
    XCTAssertEqualObjects([countryInfo fullName], @"Spain (España)", @"fullName failed.");
    XCTAssertEqualObjects([countryInfo code], @"ES", @"code failed.");
    XCTAssertEqualObjects([countryInfo capital], @"Madrid", @"capital failed.");
    XCTAssertEqualObjects([countryInfo formattedPopulation], @"46,439,864", @"population failed.");
    XCTAssertEqualObjects([countryInfo fullRegion], @"Europe (Southern Europe)", @"fullRegion failed.");
}

@end
