
RebtelAssigment
============
RebtelAssignment is an assignment for a company called Rebtel. Basically it shows how to donwnload country information from an API and show this information on the screen. It downloads a list of country info and the flag, shows this list on the main screen with the possibility to show more country details.


Git Repository
===============

<https://ginesinhoCT@bitbucket.org/ginesinhoCT/rebtelassignment.git>

Country API
============

The link to the countries API is:

<https://restcountries.eu/>

The link to the repository with the country flags is:

<https://github.com/hjnilsson/country-flags>

Networking
============

I use AFNetworking framework for implementing the calls to the API and flags repository. For the Internet Reachability I use the Apple Reachibility implementation. I only check the internet connection when I am going to call the API and show an alert view in the case there is not Internet connection. I am not monitoring it.  

Localization
===========

This project is localizated to two languajes: English and Spanish. The Localizable.string files are in Resources/Strings.

Model
======

The model only have the CountryInfo class. It has a public and private interface. The private interface properties are used to create the object (init). The publice interface properties are computed properties and they are used to provide the right data to the view controller.

This implementation is based on the MVVM design pattern where the public interface of the class is the View Model and the data that provides is tested in the Unit tests.

Views
=====

We don't use storyboards in the application because the company, probably, is still using xib files.

The Main View is a tableView with the list of the countries (flag and name).

CountryDetailsViewController is a view that show more information about the country (flag, name, original name, population, capital, region).

The desing of the app is very simple because there wasn't any design reference in the assigment. To show how I can implement UI's, in the Detail View, I use size classes for customizing:

    - wC hC
    - wC hR


Using the Sample
================

Build and run the sample using Xcode:


    A - If there is internet connection:
        
        1. The list of the countries is loaded.
        2. Click on one country cell.

        - If there is internet connection:  
            3. The Country Detail view is showed.

        - If there is not internet connection:
            3. An alert view shows the error.

        4. Come back to Main View.

            - If there is internet connection:  
                5. The Country list is reload it if it's necesarry.

            - If there is not internet connection:
                5. An alert view shows the error.
        
    B - If there is not internet connection:

        1. An alert view shows the error.
        2. The list of the countries is loaded with information that was cached. If there is not cached information it shows an empty table with a message.

        3. This step and the next one are the same than in the A.  



Main Files
==========

MainTableViewController.{h,m, xib}
 -Main view controller that displays the list of the countries in a tableView.

CountryDetailsViewController.{h,m, xib}
- Simple view controller that displays for information about the selected country.

RBTHTTPCountriesClient.{h,m}
- Client used for api networking.

RBTCheckReachability.{h,m}
- Code that checks if there is internet connection or not.

RBTCountryInfo.{h,m}
- Country Info Class.


=============================================
Copyright (C) Ginés Sánchez. All rights reserved.

