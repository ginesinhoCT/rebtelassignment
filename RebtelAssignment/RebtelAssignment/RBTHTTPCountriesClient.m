//
//  RBTHTTPClient.m
//  RebtelAssignment
//
//  Created by Gines Sanchez Merono on 17/1/17.
//  Copyright © 2017 Ginés Sanchez. All rights reserved.
//

#import "RBTHTTPCountriesClient.h"
#import "RBTConstants.h"
#import <AFNetworkActivityIndicatorManager.h>

static RBTHTTPCountriesClient *_sharedClient = nil;
static dispatch_once_t onceToken;

@interface RBTHTTPCountriesClient ()

@end

@implementation RBTHTTPCountriesClient

#pragma mark - Initialization

+ (instancetype)sharedClient {
    dispatch_once(&onceToken, ^{
        // session configuration setup
        NSURLSessionConfiguration *sessionConfiguration = [NSURLSessionConfiguration defaultSessionConfiguration];                
        
        //Uncomment this if you want to use a cache
        // cache
        NSURLCache *cache = [[NSURLCache alloc] initWithMemoryCapacity:5 * 1024 * 1024     // 5MB. memory cache
                                                          diskCapacity:20 * 1024 * 1024    // 20MB. on disk cache
                                                              diskPath:nil];
        sessionConfiguration.URLCache = cache;
        sessionConfiguration.requestCachePolicy = NSURLRequestUseProtocolCachePolicy;
        
        // initialize the session
        _sharedClient = [[RBTHTTPCountriesClient alloc] initWithBaseURL:[NSURL URLWithString:kwCountriesAPIBaseUrl] sessionConfiguration:sessionConfiguration];
        
        // Network activity indicator manager setup
        [[AFNetworkActivityIndicatorManager sharedManager] setEnabled:YES];
    });    
    return _sharedClient;
}

+ (void)resetClient {
    _sharedClient = nil;
    onceToken = 0;
}

-(void) getAllCountriesInfoRequestWithSucess:(SuccessArrayResponse)success andFailure:(Failure)failure {
    
    NSString *stringURL = [NSString stringWithFormat:@"%@/all", kwCountriesAPIBaseUrl];
    NSURL *URL = [NSURL URLWithString:stringURL];
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    [manager GET:URL.absoluteString parameters:nil progress:nil success:^(NSURLSessionTask *task, id responseObject) {        
        success ([responseObject copy]);
    } failure:^(NSURLSessionTask *operation, NSError *error) {
        failure([error localizedDescription]);
    }];    
}


@end
