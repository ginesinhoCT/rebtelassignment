//
//  CountryDetailsViewController.m
//  RebtelAssignment
//
//  Created by Gines Sanchez Merono on 18/1/17.
//  Copyright © 2017 Ginés Sanchez. All rights reserved.
//

#import "CountryDetailsViewController.h"
#import "RBTConstants.h"
#import "UIImageView+AFNetworking.h"
#import "NSString+Location.h"
#import "RBTCheckReachability.h"
#import "RBTAlerts.h"

@interface CountryDetailsViewController ()

@property (weak, nonatomic) IBOutlet UIImageView *countryFlagimage;
@property (weak, nonatomic) IBOutlet UILabel *fullNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *countryCodeLabel;
@property (weak, nonatomic) IBOutlet UILabel *capitalLabel;
@property (weak, nonatomic) IBOutlet UILabel *populationLabel;
@property (weak, nonatomic) IBOutlet UILabel *regionLabel;


@end

@implementation CountryDetailsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    [self setUpNavigationBar];
    [self setUpLabels];
    [self setUpImages];
}

-(void) viewWillAppear:(BOOL)animated {
    
    if (![RBTCheckReachability available]) {
        [RBTAlerts showErrorAlertWithTitle: NSLocalizedString(@"Error", nil)
                                   message: NSLocalizedString(@"ErrorNotInternetCountryDetail", nil)
                               andDelegate:self];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Set up

-(void) setUpNavigationBar {    
    self.navigationItem.title = NSLocalizedString(@"DetailTitle", nil);
}


-(void) setUpLabels {
        
    self.fullNameLabel.text = [self.countryInfo fullName];
    self.countryCodeLabel.text = [[self.countryInfo code] localizeLabel:@"CountryCode"];
    self.capitalLabel.text = [[self.countryInfo capital] localizeLabel:@"CountryCapital"];
    self.populationLabel.text = [[self.countryInfo formattedPopulation] localizeLabel:@"CountryPopulation"];
    self.regionLabel.text = [[self.countryInfo fullRegion] localizeLabel:@"CountryRegion"];
}

-(void) setUpImages {
    NSString * urlString = [[kwCountryFlags1000pxBaseUrl stringByReplacingOccurrencesOfString:kwIsoCountryCode
                                                                                  withString:[self.countryInfo code]] lowercaseString];
    [self.countryFlagimage setImageWithURL:[NSURL URLWithString:urlString]];
}

@end
