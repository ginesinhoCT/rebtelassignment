//
//  RBTCheckReachability.h
//  RebtelAssignment
//
//  Created by Gines Sanchez Merono on 25/1/17.
//  Copyright © 2017 Ginés Sanchez. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface RBTCheckReachability : NSObject

/**
 *  Checks if there is internet connection or not.
 *  @return BOOL that tells if there is internet connection or not
 */
+(BOOL) available;

@end
