//
//  RBTConstants.m
//  RebtelAssignment
//
//  Created by Gines Sanchez Merono on 17/1/17.
//  Copyright © 2017 Ginés Sanchez. All rights reserved.
//

#import "RBTConstants.h"

@implementation RBTConstants

#pragma mark - URL API

NSString *const kwCountriesAPIBaseUrl = @"https://restcountries.eu/rest/v1";
NSString *const kwCountryFlags1000pxBaseUrl = @"https://raw.githubusercontent.com/hjnilsson/country-flags/master/png1000px/isoCountryCode.png";
NSString *const kwCountryFlags250pxBaseUrl = @"https://raw.githubusercontent.com/hjnilsson/country-flags/master/png250px/isoCountryCode.png";

NSString *const kwIsoCountryCode = @"isoCountryCode";

#pragma mark - Country Info

NSString *const kwCountryName       = @"name";
NSString *const kwCountryNativeName = @"nativeName";
NSString *const kwCountryCode       = @"alpha2Code";
NSString *const kwCountryCapital    = @"capital";
NSString *const kwCountryPopulation = @"population";
NSString *const kwCountryRegion     = @"region";
NSString *const kwCountrySubRegion  = @"subregion";


@end
