//
//  RBTConstants.h
//  RebtelAssignment
//
//  Created by Gines Sanchez Merono on 17/1/17.
//  Copyright © 2017 Ginés Sanchez. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface RBTConstants : NSObject

#pragma mark - URL API

/**
 *  URL string with the countries API base url
 */
extern NSString *const kwCountriesAPIBaseUrl;

/**
 *  URL string with the country flags base url (image size 1000px)
 */
extern NSString *const kwCountryFlags1000pxBaseUrl;

/**
 *  URL string with the country flags base url (image size 250px)
 */
extern NSString *const kwCountryFlags250pxBaseUrl;

/**
 *  String used to constumize country flag base url with the country code
 */
extern NSString *const kwIsoCountryCode;

#pragma mark - Country Info

/**
 *  Country name
 */
extern NSString *const kwCountryName;

/**
 *  Country native name
 */
extern NSString *const kwCountryNativeName;

/**
 *  Country ISO code
 */
extern NSString *const kwCountryCode;

/**
 *  Country capital
 */
extern NSString *const kwCountryCapital;

/**
 *  Country population
 */
extern NSString *const kwCountryPopulation;

/**
 *  Country region
 */
extern NSString *const kwCountryRegion;

/**
 *  Country SubRegion
 */
extern NSString *const kwCountrySubRegion;


@end
