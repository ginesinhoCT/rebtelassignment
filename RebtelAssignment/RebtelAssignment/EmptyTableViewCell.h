//
//  EmptyTableViewCell.h
//  RebtelAssignment
//
//  Created by Gines Sanchez Merono on 18/1/17.
//  Copyright © 2017 Ginés Sanchez. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface EmptyTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *titleLabel;

+ (CGFloat)cellHeight;
+ (NSString *)cellReuseIdentifier;


@end
