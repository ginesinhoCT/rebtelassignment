//
//  MainTableViewController.m
//  RebtelAssignment
//
//  Created by Gines Sanchez Merono on 17/1/17.
//  Copyright © 2017 Ginés Sanchez. All rights reserved.
//

#import "MainTableViewController.h"
#import "AFNetworking.h"
#import "RBTConstants.h"
#import "RBTHTTPCountriesClient.h"
#import "RBTCountryInfo.h"
#import "UIImageView+AFNetworking.h"
#import "CountryDetailsViewController.h"
#import "CountryTableViewCell.h"
#import "EmptyTableViewCell.h"
#import "RBTAlerts.h"
#import "RBTCheckReachability.h"


@interface MainTableViewController (){
    
    NSArray *countryInfoArray;
}

@end

@implementation MainTableViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self setUpViewController];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void) viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    //Reload the table if it's necessary
    if ([RBTCheckReachability available]) {
        [self downloadCountryInfoArray];
    }else {        
        [RBTAlerts showErrorAlertWithTitle: NSLocalizedString(@"Error", nil)
                                   message: NSLocalizedString(@"ErrorNotInternetCountryList", nil)
                               andDelegate:self];
    }
}

#pragma mark - Set up

-(void) setUpViewController{
    
    [self setUpNavigationBar];
    [self setUpArray];
    [self registerNibs];
}

-(void) setUpNavigationBar {    
    self.navigationItem.title = NSLocalizedString(@"MainTitle", nil);
}

-(void) setUpArray {
    countryInfoArray = [NSArray new];
}

- (void)registerNibs {
    [self.tableView registerNib:[UINib nibWithNibName:@"CountryTableViewCell" bundle:nil] forCellReuseIdentifier:[CountryTableViewCell cellReuseIdentifier]];
    [self.tableView registerNib:[UINib nibWithNibName:@"EmptyTableViewCell" bundle:nil] forCellReuseIdentifier:[EmptyTableViewCell cellReuseIdentifier]];
}


#pragma mark - Networking

-(void) downloadCountryInfoArray {    
    [[RBTHTTPCountriesClient sharedClient] getAllCountriesInfoRequestWithSucess: ^(NSArray *responseArray) {
        countryInfoArray = [responseArray copy];
        [self.tableView reloadData];
    } andFailure:^(NSString *errorString) {
        [RBTAlerts showErrorAlertWithTitle:NSLocalizedString(@"Error", nil) message:errorString andDelegate:self];        
    }];
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if ([countryInfoArray count] > 0) {
        self.tableView.userInteractionEnabled = YES;
        return [countryInfoArray count];
    }else {
        self.tableView.userInteractionEnabled = NO;
        return 1;
    }
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {

    if ([countryInfoArray count] > 0) {
        CountryTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:[CountryTableViewCell cellReuseIdentifier] forIndexPath:indexPath];
        
        // Configure the cell...
        RBTCountryInfo *countryInfo = [[RBTCountryInfo alloc] initWithCountryInfo:countryInfoArray[indexPath.row]];
        cell.namelLabel.text = [countryInfo name];
        
        NSString * urlString = [[kwCountryFlags250pxBaseUrl stringByReplacingOccurrencesOfString:kwIsoCountryCode
                                                                                      withString:[countryInfo code]] lowercaseString];        
        [cell.flagImage setImageWithURL: [NSURL URLWithString:urlString]
                       placeholderImage: [UIImage imageNamed:@"defaultFlag"]];
        
        return cell;
    }else {
        //Show message when table is empty
        EmptyTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:[EmptyTableViewCell cellReuseIdentifier] forIndexPath:indexPath];
        cell.titleLabel.text = NSLocalizedString(@"CountryListNotReady", nil);
        return cell;
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    //Add space in the top of the table view
    return 1.0;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    //Add space in the bottom of the table view
    return 4.0;
}



#pragma mark - Table view delegate

// In a xib-based application, navigation from a table can be handled in -tableView:didSelectRowAtIndexPath:
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    // Navigation logic may go here, for example:
    // Create the next view controller.
    
    CountryDetailsViewController *detailViewController = [[CountryDetailsViewController alloc] initWithNibName:@"CountryDetailsViewController" bundle:nil];
    detailViewController.countryInfo = [[RBTCountryInfo alloc] initWithCountryInfo:countryInfoArray[indexPath.row]];
    
    // Pass the selected object to the new view controller.
    
    // Push the view controller.
    [self.navigationController pushViewController:detailViewController animated:YES];
}


@end
