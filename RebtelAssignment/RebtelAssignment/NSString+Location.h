//
//  NSString+Location.h
//  RebtelAssignment
//
//  Created by Gines Sanchez Merono on 19/1/17.
//  Copyright © 2017 Ginés Sanchez. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString (Location)

-(NSString *) localizeLabel: (NSString *) labelKey;

@end
