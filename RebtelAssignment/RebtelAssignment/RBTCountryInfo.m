//
//  RBTCountryInfo.m
//  RebtelAssignment
//
//  Created by Gines Sanchez Merono on 17/1/17.
//  Copyright © 2017 Ginés Sanchez. All rights reserved.
//

#import "RBTCountryInfo.h"
#import "RBTConstants.h"

@interface RBTCountryInfo()

@property (nonatomic) NSString * name;
@property (nonatomic) NSString * nativeName;
@property (nonatomic) NSString * code;
@property (nonatomic) NSString * capital;
@property (nonatomic) NSNumber * population;
@property (nonatomic) NSString * region;
@property (nonatomic) NSString * subregion;

@end

@implementation RBTCountryInfo

#pragma mark - Initializers

- (id)init {
    return [super init];
}

-(id) initWithCountryInfo: (NSDictionary *) countryInfo {
    
    return [self initWithName:countryInfo[kwCountryName]
                   nativeName:countryInfo[kwCountryNativeName]
                         code:countryInfo[kwCountryCode]
                      capital:countryInfo[kwCountryCapital]
                   population:countryInfo[kwCountryPopulation]
                       region:countryInfo[kwCountryRegion]
                    subregion:countryInfo[kwCountrySubRegion]];
}

- (id)initWithName:(NSString *)name nativeName:(NSString *)nativeName code:(NSString *) code capital:(NSString *) capital
        population:(NSNumber *) population region:(NSString *) region subregion:(NSString *) subregion {
    
    self = [super init];
    if(self) {
        _name = name;
        _nativeName = nativeName;
        _code = code;
        _capital = capital;
        _population = population;
        _region = region;
        _subregion = subregion;
    }
    return self;
}

#pragma mark - Public Interface

-(NSString *) name {
    return  _name;
}

-(NSString *) fullName {
    return  [NSString stringWithFormat:@"%@ (%@)", _name, _nativeName];
}

-(NSString *) code {
    return _code;
}

-(NSString *) capital {
    return _capital;
}

-(NSString *) formattedPopulation {
    NSNumberFormatter *formatter = [[NSNumberFormatter alloc] init];
    [formatter setNumberStyle:NSNumberFormatterDecimalStyle];
    return [formatter stringForObjectValue:_population];
}


-(NSString *) fullRegion {
    return [NSString stringWithFormat: @"%@ (%@)", _region, _subregion];
}

@end
