//
//  CountryDetailsViewController.h
//  RebtelAssignment
//
//  Created by Gines Sanchez Merono on 18/1/17.
//  Copyright © 2017 Ginés Sanchez. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RBTCountryInfo.h"

@interface CountryDetailsViewController : UIViewController

@property (nonatomic) RBTCountryInfo *countryInfo;

@end
