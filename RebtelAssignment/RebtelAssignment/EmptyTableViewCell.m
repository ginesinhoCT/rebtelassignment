//
//  EmptyTableViewCell.m
//  RebtelAssignment
//
//  Created by Gines Sanchez Merono on 18/1/17.
//  Copyright © 2017 Ginés Sanchez. All rights reserved.
//

#import "EmptyTableViewCell.h"

@implementation EmptyTableViewCell

#pragma mark - Class methods

+ (CGFloat)cellHeight {
    return 50.0;
}

+ (NSString *)cellReuseIdentifier {
    return @"EmptyCell";
}


#pragma mark - Lifecycle

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    
    // Configure the view for the selected state
}

@end
