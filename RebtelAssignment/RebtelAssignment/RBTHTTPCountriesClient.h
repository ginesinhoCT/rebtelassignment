//
//  RBTHTTPClient.h
//  RebtelAssignment
//
//  Created by Gines Sanchez Merono on 17/1/17.
//  Copyright © 2017 Ginés Sanchez. All rights reserved.
//

#import <AFNetworking/AFNetworking.h>

@interface RBTHTTPCountriesClient : AFHTTPSessionManager

/**
 *  Handling API responses for endpoind calls
 *
 *  @param responseArray response from API
 */
typedef void(^SuccessArrayResponse)(NSArray *responseArray);

/**
 *  Handling API errors for endpoind calls
 *
 *  @param errorLocalizedDescriptionString localized error from API
 */
typedef void(^Failure)(NSString *errorLocalizedDescriptionString);

/**
 *  Singleton for creating a share instance in project
 *
 *  @return HTTPClient instance
 */
+ (instancetype)sharedClient;

/**
 *  Testing
 *
 */
-(void) getAllCountriesInfoRequestWithSucess:(SuccessArrayResponse)success andFailure:(Failure)failure;

@end
