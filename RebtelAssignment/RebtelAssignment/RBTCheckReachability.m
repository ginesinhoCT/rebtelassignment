//
//  RBTCheckReachability.m
//  RebtelAssignment
//
//  Created by Gines Sanchez Merono on 25/1/17.
//  Copyright © 2017 Ginés Sanchez. All rights reserved.
//

#import "RBTCheckReachability.h"
#import "Reachability.h"

@implementation RBTCheckReachability

+(BOOL) available {
    
    Reachability *internetReachability = [Reachability reachabilityForInternetConnection];
    [internetReachability startNotifier];
    
    if ([internetReachability currentReachabilityStatus] == NotReachable) {
        NSLog(@"There is not Internet connection");
        return NO;
    }else {
        NSLog(@"There is Internet connection");
        return YES;
    }
}

@end
