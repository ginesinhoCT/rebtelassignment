//
//  ATAAlerts.m
//  RebtelAssignment
//
//  Created by Gines Sanchez Merono on 25/1/17.
//  Copyright © 2017 Ginés Sanchez. All rights reserved.
//

#import "RBTAlerts.h"

@implementation RBTAlerts

+(void) showErrorAlertWithTitle: (NSString *) title message:(NSString *) message andDelegate: (id) delegate {
    
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:title
                                        message:message
                                 preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction *buttonAction = [UIAlertAction actionWithTitle:NSLocalizedString(@"OK", nil)
                                                          style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {
                                                              
                                                          }];
    [alert addAction:buttonAction];
    
    [delegate presentViewController:alert animated:YES completion:nil];
}

@end
