//
//  RBTCountryInfo.h
//  RebtelAssignment
//
//  Created by Gines Sanchez Merono on 17/1/17.
//  Copyright © 2017 Ginés Sanchez. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface RBTCountryInfo : NSObject

#pragma mark - Initializers

/**
 *  Create a RBTCountryInfo object with a specific country info dictionary
 *
 *  @param countryInfo  dictionary with the country info
 */
-(id) initWithCountryInfo: (NSDictionary *) countryInfo;

/**
 *  Designated Initializer: RBTCountryInfo object wit the next parameters
 *
 *  @param name         string with the country name
 *  @param nativeName   string with the country native name
 *  @param code         string with the country ISO code
 *  @param capital      string with the country capital
 *  @param population   string with the country population
 *  @param region       string with the country region
 *  @param subregion    string with the country subregion
 */
- (id)initWithName:(NSString *) name
        nativeName:(NSString *) nativeName
              code:(NSString *) code
           capital:(NSString *) capital
        population:(NSNumber *) population
            region:(NSString *) region
         subregion:(NSString *) subregion;



#pragma mark - Public Interface

/**
 *  Return country name in English
 */
-(NSString *) name;

/**
 *  Return country full name: name(nativeName)
 */
-(NSString *) fullName;

/**
 *  Return country code
 */
-(NSString *) code;

/**
 *  Return country capital
 */
-(NSString *) capital;

/**
 *  Return formatted population (45,344,234)
 */
-(NSString *) formattedPopulation;

/**
 *  Return full region (Americas (South America()
 */
-(NSString *) fullRegion;

@end
