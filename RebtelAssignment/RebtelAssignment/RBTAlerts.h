//
//  RBTAlerts.h
//  RebtelAssignment
//
//  Created by Gines Sanchez Merono on 25/1/17.
//  Copyright © 2017 Ginés Sanchez. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface RBTAlerts : NSObject

/**
 *  Show an Error alert
 *
 *  @param title string
 *  @param message string
 */
+(void) showErrorAlertWithTitle: (NSString *) title message:(NSString *) message andDelegate: id;

@end
