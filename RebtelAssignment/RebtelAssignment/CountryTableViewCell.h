//
//  CountryTableViewCell.h
//  RebtelAssignment
//
//  Created by Gines Sanchez Merono on 18/1/17.
//  Copyright © 2017 Ginés Sanchez. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CountryTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *namelLabel;
@property (weak, nonatomic) IBOutlet UIImageView *flagImage;

+ (CGFloat)cellHeight;
+ (NSString *)cellReuseIdentifier;

@end
