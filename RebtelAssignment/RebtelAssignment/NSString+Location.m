//
//  NSString+Location.m
//  RebtelAssignment
//
//  Created by Gines Sanchez Merono on 19/1/17.
//  Copyright © 2017 Ginés Sanchez. All rights reserved.
//

#import "NSString+Location.h"

@implementation NSString (Location)

-(NSString *) localizeLabel: (NSString *) labelKey {    
    NSString * string = [NSString stringWithFormat:NSLocalizedString(labelKey, nil), self];    
    return string;
}

@end
